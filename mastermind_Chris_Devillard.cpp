#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct InfoJoueur
{
    int nbEssai;
    int partieGagne;
    int partiePerdue;

};


int choixContinue(int continuer)  //Fonction pour savoir si le joueur souhaite continuer ou non
{
    std::cout<<"Voulez vous continuez ? Tapez [1] pour continuer, [0] pour arreter"<< std::endl;
    std::cin>> continuer;
    while (continuer >1 || continuer < 0)
    {
        std::cout<<"Vous avez entrez un mauvais chiffre"<< std::endl;
        std::cout<<"Voulez vous continuez ? Tapez [1] pour continuer, [0] pour arreter"<< std::endl;
        std::cin>> continuer;
    }
    return continuer;
}

int testCombi (int tabOrdi[4], int tabJoueur[4], int nbEssai, int bienPlace) // Sert a tester si les combinaisons sont bonnes
{
    for (int i = 0; i < 4 ; ++i)
    {
        if (tabOrdi[i] == tabJoueur[i])  // si les deux chiffres au meme emplacement sont bon :
        {
            std::cout<<"Votre chiffre " << tabJoueur[i]<< " sur la case "<< i+1 <<" est bien place"<<std::endl;
            ++bienPlace;
        }

        else if (tabOrdi[i] != tabJoueur[i]) //Si un chiffre existe mais est dans la mauvaise position :
        {
            for (int j = 0; j < 4; ++j)
            {
                if (tabOrdi[i] == tabJoueur[j])
                {
                    std::cout<<"Le chiffre "<< tabOrdi[i] <<" en position "<< j+1 <<" existe mais a un autre emplacement."<<std::endl;
                }

            }
        }
    }
    nbEssai--; // reduction du nombre d'essai a chaque fois qu'on passe dans la fonction
    if (bienPlace >= 4) // si les 4 pions sont bien plac�s, nbEssai = 11, ce qui permet de quitter la boucle qui renvoie dans la fonction et permet de savoir que le joueur a gagn�
    {
        nbEssai = 11;
    }
    return nbEssai;
}


int creeChoixJoueur () // Cette fonction sert a remplir le tableau "choixJoueur[4]"
{
    int nbJoueur = 0;
    std::cout<<"Entrez un chiffre entre 0 et 9"<<std::endl;
    std::cin>>nbJoueur;

    while (nbJoueur > 9 || nbJoueur < 0)
    {
        std::cout<<"Il faut rentrer un chiffre entre 0 et 9"<<std::endl;
        std::cin>>nbJoueur;
    }
    return nbJoueur;
}


int main()
{
    srand(time(NULL));

    InfoJoueur JoueurUn;
    JoueurUn.partieGagne = 0;
    JoueurUn.partiePerdue = 0;

    int choixJoueur[4]; // les choix du joueur dans un tableau
    int choixOrdi[4]; // Les 4 chiffres de l'ordi que l'on va devoir trouver
    bool continuer = true;

    while (continuer == true)
    {
        JoueurUn.nbEssai = 10; // Initialisation du nombre de chance, si 0, partie termin�e
        int bienPlace = 0; // initialisation du nombre de pion bien plac�

        for (int i = 0; i<4; ++i ) //G�n�ration des nombres de l'ordi
        {
            choixOrdi[i] = rand()%10;
            // std::cout<<"l'ordi choisi "<<choixOrdi[i]<<std::endl; // afficher les choix de l'ordi, pour le debug
        }

        while (JoueurUn.nbEssai > 0 && JoueurUn.nbEssai < 11) //Si nbEssai en dessous de 0, on sort de la boucle parce que le joueur a perdu. Si nbEssai au dessus de 10, le joueur gagne
        {
            for (int i = 0; i<4 ; ++i) //Re-Selection du choix du joueur
            {
                choixJoueur[i]=creeChoixJoueur(); //Je remplis a nouveau le tableau des choix du joueur
            }
            JoueurUn.nbEssai=testCombi(choixOrdi, choixJoueur, JoueurUn.nbEssai, bienPlace);
        }

        if (JoueurUn.nbEssai > 10)  //si le nombre d'essai est sup�rieur a 10, donc que le joueur a gagn�
        {
            std::cout<< "Vous avez gagnez la partie !"<< std::endl;
            ++JoueurUn.partieGagne;
        }

        else if (JoueurUn.nbEssai <= 0) // si le joueur perd
        {
            std::cout<< "Vous avez perdu"<<std::endl;
            ++JoueurUn.partiePerdue;
        }

        std::cout<< "Vous avez joue "<< JoueurUn.partieGagne+JoueurUn.partiePerdue<<" fois"<<std::endl; // J'informe le joueur du nombre de partie gagn�e/perdue et le nombre de partie jou�es
        std::cout<< "Vous avez vaincu l'ordi "<< JoueurUn.partieGagne << " fois et en avez perdu " << JoueurUn.partiePerdue << " fois"<<std::endl;

        continuer = choixContinue(continuer); //demande au joueur si il souhaite continuer
    }

    return 0;
}
